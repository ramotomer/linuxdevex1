#include "main.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <string.h>

#define MAX_READ_SIZE (2048)
#define MAX_PROCESS_COUNT (131072)
#define MAX_PROCESS_INFORMATION_SIZE (512)
#define NO_ERROR (0)
#define ERROR_OCCURRED (1)

#define TRUE (1)
#define FALSE (0)

#define LINE_FEED_CHAR (10)


int main() {
    print_processes();
    return NO_ERROR;
}


ErrorCode print_processes() {
    int* pids = malloc(MAX_PROCESS_COUNT * sizeof(int));  // an array of all of the PID-s of the computer's processes.
    if (NULL == pids) {
        printf("failed while allocating PID array\n");
        return ERROR_OCCURRED;
    }

    if (NO_ERROR != get_all_PIDs(pids)) {
        printf("failed at getting all process IDs\n");
        goto error;
    }

    for(int i = 0; i < MAX_PROCESS_COUNT; i++) {
        if (0 == pids[i]) {
            break;
        }
        print_process_data_line(pids[i]);
    }

    free(pids);
    return NO_ERROR;
error:
    free(pids);
    return ERROR_OCCURRED;
}


ErrorCode print_process_data_line(int pid) {
    char* process_bin = malloc(MAX_PROCESS_INFORMATION_SIZE);
    char* process_name = malloc(MAX_PROCESS_INFORMATION_SIZE);
    char* process_args = malloc(MAX_PROCESS_INFORMATION_SIZE);
    if (NULL == process_bin) {
        printf("failed at allocating memory for process binary path\n");
        goto error_at_process_bin;
    }
    if (NULL == process_name) {
        printf("failed at allocating memory for process name");
        goto error_at_process_name;
    }
    if (NULL == process_args) {
        printf("failed at allocating memory for process arguments.\n");
        goto error_at_process_args;
    }

    if (NO_ERROR != process_binary_path_from_pid(pid, process_bin)) {
        printf("failed to get process binary path\n");
        goto error;
    }
    if (NO_ERROR != process_name_from_pid(pid, process_name)) {
        printf("failed to get process name\n");
        goto error;
    }
    if (NO_ERROR != process_arguments_from_pid(pid, process_args)) {
        printf("failed to get process arguments\n");
        goto error;
    }

    printf("PID: %d, Name: %s, binary path: %s, arguments: %s\n", pid, process_name, process_bin, process_args);

    free(process_args);
    free(process_name);
    free(process_bin);
    return NO_ERROR;
error:
    free(process_args);
error_at_process_args:
    free(process_name);
error_at_process_name:
    free(process_bin);
error_at_process_bin:
    return ERROR_OCCURRED;
}


char* trim_from_right(char* s) {
    char* back = s + strlen(s);
    while(!isgraph(*(--back)));
    *(back + 1) = '\0';
    return s;
}


char* translate(char* string, char old_char, char new_char) {
    int i = 0;
    while ('\0' != string[i]) {
        if (old_char == string[i]) {
            string[i] = new_char;
        }
        i++;
    }
    return string;
}


int is_num(char* string) {
    for (int i = 0; i < strlen(string); i++) {
        if (string[i] < '0' || string[i] > '9') {
            return FALSE;
        }
    }
    return TRUE;
}

char* terminate_string_at_char(char* string, char c) {
    int i = 0;
    while (c != string[i] && i < strlen(string)) {
        i++;
    }
    string[i] = '\0';
    return string;
}


ErrorCode get_all_PIDs(int* pids) {
    DIR* proc_dir = NULL;
    struct dirent* dir_item = NULL;

    proc_dir =  opendir("/proc");
    if (NULL == proc_dir) {
        printf("failed at opening the directory\n");
        goto error;
    }

    dir_item = readdir(proc_dir);
    int list_index = 0;
    while (NULL != dir_item) {
        if (is_num(dir_item->d_name)) {
            sscanf(dir_item->d_name, "%d", (pids + list_index));  //string to integer inside `pid`.
            list_index ++;
        }
        dir_item = readdir(proc_dir);
    }

    return NO_ERROR;
error:
    return ERROR_OCCURRED;
}


ErrorCode process_arguments_from_pid(int pid, char* arguments) {
    if (NO_ERROR != read_process_file(pid, "cmdline", arguments)) {
        printf("failed to read the process's file\n");
        goto error;
    }

    if ('\0' == *arguments) {  // no arguments
        return NO_ERROR;
    }

    // returned arguments are separated by '\0's and end with two '\0's
    int i = 0;
    while (!('\0' == arguments[i] && '\0' == arguments[i+1])) {
        if ('\0' == arguments[i]) {
            arguments[i] = ' ';   //turn '\0's to spaces.
        }
        i++;
    }

    terminate_string_at_char(arguments, 16);  // for some reason if the file is empty a weird unprintable string is returned and it starts with '\16'

    return NO_ERROR;
error:
    return ERROR_OCCURRED;
}


ErrorCode process_binary_path_from_pid(int pid, char* binary_path) {
    if (NO_ERROR != read_process_link_from_pid(pid, "exe", binary_path)) {
        printf("could not read the process's link\n");
        goto error;
    }

    return NO_ERROR;
error:
    return ERROR_OCCURRED;
}


ErrorCode process_name_from_pid(int pid, char* name) {
    if (NO_ERROR != read_process_file(pid, "comm", name)) {
        printf("could not read process file!\n");
        goto error;
    }

    return NO_ERROR;
error:
    return ERROR_OCCURRED;
}


ErrorCode read_process_file(int pid, char* filename, char* value) {
    char* file_location = NULL;
    int file_descriptor = -1;

    file_location = malloc(MAX_PROCESS_INFORMATION_SIZE);
    if (NULL == file_location) {
        printf("failed at memory allocation.\n");
        goto error_no_free;
    }

    if (-1 == sprintf(file_location, "/proc/%d/%s", pid, filename)) {
        printf("failed at string foramtting.\n");
        goto error;
    }

    file_descriptor = open(file_location, O_RDONLY);
    if (-1 == file_descriptor) {
        printf("failed at file opening. error num: %d\n", errno);
        if (ENOENT == errno) {  // no such file
            printf("no such file as \"%s\".\n", file_location);
        }
        goto error;
    }

    if (-1 == read(file_descriptor, value, MAX_READ_SIZE)) {
        printf("failed at file reading.\n");
        goto error;
    }

    terminate_string_at_char(value, LINE_FEED_CHAR);   // we want to end the string after we a LINE_FEED (LF) because of the special files in /proc.

    if (-1 == close(file_descriptor)) {
        printf("failed at file closing.\n");
        goto error;
    }

    free(file_location);
    return NO_ERROR;
error:
    free(file_location);
error_no_free:
    return ERROR_OCCURRED;
}

ErrorCode read_process_link_from_pid(int pid, char* link_name, char* value) {
    char* file_location = NULL;

    file_location = malloc(MAX_PROCESS_INFORMATION_SIZE);
    if (NULL == file_location) {
        printf("failed at memory allocation.\n");
        goto error_no_free;
    }

    if (-1 == sprintf(file_location, "/proc/%d/%s", pid, link_name)) {
        printf("failed at string formatting.\n");
        goto error;
    }

    if (-1 == readlink(file_location, value, MAX_PROCESS_INFORMATION_SIZE)) {
        if (ENOENT != errno) {
            printf("Failed at reading the link %s\n", file_location);
            goto error;
        }
        *value = (char)0;  // the empty string, since many processes do not have an executable.
    }

    terminate_string_at_char(value, ' ');

    free(file_location);
    return NO_ERROR;
error:
    free(file_location);
error_no_free:
    return ERROR_OCCURRED;
}
