//
// Created by osboxes on 4/22/20.
//

#ifndef PSPS_MAIN_H
#define PSPS_MAIN_H

typedef int ErrorCode;

/***
 * Prints out all of the processes in the computer.
 * Acts like the `ps` function.
 * @return
 */
ErrorCode print_processes();

/***
 *  Prints out one line of the 'ps' output, it receives a PID and prints the matching line.
 * @param pid [IN]: the PID of the process to print.
 * @return: an error code to whether the function succeeded.
 */
ErrorCode print_process_data_line(int pid);

/***
 *
 * @param string
 * @return
 */
char* trim_from_right(char* string);

/***
 *  Translates every apperance of a character in a string to another character.
 * @param string [IN\OUT]: the string.
 * @param old_char [IN]: the char to replace.
 * @param string [IN]: the char to replace it with.
 * @return a point to the string.
 */
char* translate(char* string, char old_char, char new_char);

/***
 * Return an int pointer of all of the PIDs that are currently running on the computer.
 * @param pids [OUT]: an int* - the output parameter.
 * @return: an error code (0 success, 1 fail)
 */
ErrorCode get_all_PIDs(int* pids);

/***
 *   Receives a string and returns whether or not that string is a number.
 * @param string: a char* of the string desired.
 * @return: TRUE (1) if True, FALSE (0) if False.
 */
int is_num(char* string);

/***
 * Terminates a string before the first apperance of a given chararcter.
 * @param string [IN\OUT] the string.
 * @param c [IN]: the char.
 * @return: the received altered string.
 */
char* terminate_string_at_char(char* string, char c);

/***
 * Returns the arguments that the process with PID of `pid` has received.
 * @param pid [IN]: the PID of the process.
 * @param arguments [OUT]: a string of the arguments.
 * @return an ErrorCode.
 */
ErrorCode process_arguments_from_pid(int pid, char* arguments);

/***
 * Receives a PID of a process and returns its binary file's path.
 *
 * @param pid [IN] int PID of the process.
 * @param binary_path [OUT] char* buffer that will be the path after the function returns.
 * @return: an ErrorCode to whether or not the function succeeded or not.
 *  0 for success, non-zero for failure.
 */
ErrorCode process_binary_path_from_pid(int pid, char* binary_path);

/***
 * Receives a PID of a process and returns its name.
 *
 * param [IN] int pid: the PID of the process.
 * param [OUT] char* name: the output name of the process.
 * return: ErrorCode, 0 upon success, on failure, nonzero.
 ***/
ErrorCode process_name_from_pid(int pid, char* name);

/***
* Receives a PID of a process and a filename and returns the content
        * of that file in /proc/<PID>
        *
        * param [IN] int pid: the PID of the process.
* param [IN] char* filename: the name of the file in /proc/<PID>/ to read.
* param [OUT] char* value: the content of the file.
* return: ErrorCode, 0 upon success, on failure, nonzero.
***/
ErrorCode read_process_file(int pid, char* filename, char* value);

/***
 *  Reads the location that a link points to and returns that as a string.
 * @param pid [IN]: the PID of the process whose link you want to read. (a link inside its /proc/<PID>/ directory)
 * @param link_name [IN]: the name of the link inside the directory.
 * @param value [OUT]: the returned location that the link points to.
 * @return: an error code (0 == success).
 */
ErrorCode read_process_link_from_pid(int pid, char* link_name, char* value);

#endif //PSPS_MAIN_H
